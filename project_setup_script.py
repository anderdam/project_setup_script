#!/usr/bin/env python3

import os
import subprocess
import requests

init = "__init__.py"

print("""
    __  __                  _       __        ______          ___               _          
   / / / /__  ________     | |     / /__     / ____/___      /   | ____ _____ _(_)___      
  / /_/ / _ \/ ___/ _ \    | | /| / / _ \   / / __/ __ \    / /| |/ __ `/ __ `/ / __ \     
 / __  /  __/ /  /  __/    | |/ |/ /  __/  / /_/ / /_/ /   / ___ / /_/ / /_/ / / / / / _ _ 
/_/ /_/\___/_/   \___/     |__/|__/\___/   \____/\____/   /_/  |_\__, /\__,_/_/_/ /_(_|_|_)
                                                                /____/                     
""")

print("This script will create a new data project folder with the following structure:\n")
print("├── src")
print("│   ├── data")
print("│   ├── features")
print("│   ├── models")
print("│   ├── notebooks")
print("│   ├── vizualization")
print(f"│   └── {init}")
print("├── utils")
print("├── .gitignore")
print("├── .pre-commit-config.yaml")
print("├── CHANGELOG")
print("├── LICENSE")
print("├── README.md")
print("├── .gitlab-ci.yml")
print("├── setup.py")
print("└── default_pipfile.toml\n")

print("Starting the process...\n")

projects_folder_path = "/home/anderdam/projects/"
os.chdir(projects_folder_path)

# Define repo name and language
repo_link = input("Qual o link do repositório que será clonado?\n-> ")
repo_name = repo_link.split("/")[-1].split(".")[0]

# Clone the repo to scripts folder
print("Cloning the repo...")
subprocess.run(["git", "clone", repo_link])

# Change directory to new cloned folder
os.chdir(repo_name)

# Create the folders and files
folders_list = [
    'src',
    'src/data',
    'src/features',
    'src/models',
    'src/notebooks',
    'src/vizualization',
    'utils'
]

try:
    for folder in folders_list:
        os.mkdir(folder)
        with open(f"{folder}/{init}", "w") as f:
            f.write("")
except FileExistsError:
    print("The folders already exist.")
    print("Exiting the script...")
    exit()

def get_raw_content(file, link) -> bytes:
    response = requests.get(link)    
    with open(file, "w") as f:
        f.write(response.text)


# Clone the files from common_files repo
files_to_clone = {
    ".gitignore": "https://gitlab.com/anderdam/common_files/-/raw/main/.gitignore?ref_type=heads",
    ".pre-commit-config.yaml": "https://raw.githubusercontent.com/pre-commit/pre-commit/main/.pre-commit-config.yaml",
    "CHANGELOG": "https://gitlab.com/anderdam/common_files/-/raw/main/CHANGELOG?ref_type=heads",
    "LICENSE": "https://gitlab.com/anderdam/common_files/-/raw/main/LICENSE?ref_type=heads",
    "README.md": "https://gitlab.com/anderdam/common_files/-/raw/main/README.md?ref_type=heads",
    ".gitlab-ci.yml": "https://gitlab.com/anderdam/common_files/-/raw/main/.gitlab-ci.yml?ref_type=heads",
    ".flake8": "https://gitlab.com/anderdam/common_files/-/raw/main/.flake8?ref_type=heads",
    "setup.py": "https://gitlab.com/anderdam/common_files/-/raw/main/setup.py?ref_type=heads",
    "pyproject.toml": "https://gitlab.com/anderdam/common_files/-/raw/main/pyproject.toml?ref_type=heads",
}

for key, value in files_to_clone.items():
    get_raw_content(key, value)
    
# Create the virtual environment using Pipenv
print("Creating the virtual environment...")
subprocess.run(["pipenv", "--python", "--three", "install"])  # Ensure Python 3 compatibility

# Activate the virtual environment
subprocess.run(["pipenv", "shell"])

dev_dependencies = [
    "autopep8",
    "black",
    "flake8",
    "isort",
    "mypy",
    "pre-commit",
    "pylint",
    "pytest",
    "safety",
    "tox",
    "wheel",
    ]

# Install the dev requirements
for dep in dev_dependencies:
    subprocess.run(["pipenv", "install", "--dev", dep])
    
# Prompt for project dependencies
project_dependencies = input("Enter project dependencies (space-separated): ").split()

# Install project dependencies
subprocess.run(["pipenv", "install", *project_dependencies])

# Initialize pre-commit hooks and commit the files
subprocess.run(["pre-commit", "install"])
subprocess.run(["pre-commit", "run", "--all-files"])
subprocess.run(["git", "status"])
subprocess.run(["git", "add", "."])
subprocess.run(["git", "commit", "-m", "Initial commit"])
# subprocess.run(["git", "push"])
    
print("Process Done!\n")

